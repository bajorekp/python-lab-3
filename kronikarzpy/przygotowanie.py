
print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets

class Z2:
	def __init__ 
		self.iris = datasets.load_iris()
		self.X = self.iris.data[:, :2]  # we only take the first two features. We could
                # avoid this ugly slicing by using a two-dim dataset
		self.y = self.iris.target
		self.h = .02  # step size in the mesh
